#!/bin/bash

set -e

if [ $# -ne 1 ]; then
    echo "usage: <new_password>"
    exit 1
fi

PASSWORD_BCRYPYT=$(python3 -c 'import bcrypt; print(bcrypt.hashpw(b"'$1'", bcrypt.gensalt(rounds=15, prefix=b"2a")).decode("ascii"))')

kubectl -n argocd patch secret argocd-secret \
  -p '{"stringData": {
    "admin.password": "'$PASSWORD_BCRYPYT'",
    "admin.passwordMtime": "'$(date +%FT%T%Z)'"
  }}'

exit 0
