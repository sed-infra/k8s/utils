#!/bin/bash

function patch_app() {
    kubectl -n argocd get apps "$1" -o yaml | yq  -y ".metadata.finalizers = []" | kubectl -n argocd apply -f -
}

apps=$(kubectl -n argocd get apps | awk '{print $1}' | tail -n +2 -)


for app in $apps; do
    patch_app "$app"
done

kubectl delete ns argocd

sleep 10

kustomize build https://gitlab.com/sed-infra/k8s/argocd.git/ | kubectl apply -f -
kubectl apply -f https://gitlab.com/sed-infra/k8s/cluster/-/raw/master/apps/managers.yml
