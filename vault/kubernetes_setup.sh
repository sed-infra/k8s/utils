#!/bin/bash

export VAULT_SECRETS_OPERATOR_NAMESPACE=$(kubectl -n vault-secrets-operator get sa vault-secrets-operator -o jsonpath="{.metadata.namespace}")
export VAULT_SECRET_NAME=$(kubectl -n vault-secrets-operator get sa vault-secrets-operator -o jsonpath="{.secrets[*]['name']}")
export SA_JWT_TOKEN=$(kubectl -n vault-secrets-operator get secret $VAULT_SECRET_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)
export SA_CA_CRT=$(kubectl -n vault-secrets-operator get secret $VAULT_SECRET_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)
export K8S_HOST=$(kubectl -n vault-secrets-operator config view --minify -o jsonpath='{.clusters[0].cluster.server}')

vault auth enable kubernetes

# Enable kv engine
vault secrets enable -path=k3s -version=2 kv
# Create a new policy
vault policy write k3s-vault-secrets-operator ./k3s-vault-secrets-operator.hcl

# Tell Vault how to communicate with the Kubernetes cluster
vault write auth/kubernetes/config \
  token_reviewer_jwt="$SA_JWT_TOKEN" \
  kubernetes_host="$K8S_HOST" \
  kubernetes_ca_cert="$SA_CA_CRT" \
  disable_iss_validation="true"

# Create a role named, 'vault-secrets-operator' to map Kubernetes Service Account to Vault policies and default token TTL
vault write auth/kubernetes/role/vault-secrets-operator \
  bound_service_account_names="vault-secrets-operator" \
  bound_service_account_namespaces="$VAULT_SECRETS_OPERATOR_NAMESPACE" \
  policies=k3s-vault-secrets-operator \
  ttl=24h
